import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { MapInfoWindow, MapMarker } from '@angular/google-maps';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { BLUE_MARKER_SRC, RED_MARKER_SRC } from '@webui/time';
import { getPropValue } from '@webui/utilities';
import { Subject, takeUntil } from 'rxjs';
import { Modal } from '../modal/modal.component';

type MapPosition = google.maps.LatLngLiteral | google.maps.LatLng;

type Marker = {
  position: MapPosition;
  iconUrl: string;
  name?: string;
  description?: string;
  id?: string;
  selected?: boolean;
};

@Component({
  selector: 'webui-fillin-map-modal',
  templateUrl: './fillin-map-modal.component.html',
  styleUrls: ['./fillin-map-modal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FillinMapComponent
  extends Modal
  implements OnInit, AfterViewInit, OnDestroy
{
  private _destroy = new Subject<void>();

  public markers!: Marker[];
  public mapCenter!: MapPosition;

  public activeMarker?: Marker;

  data!: any[];
  supportData!: {
    __str__: string;
    latitude: number;
    longitude: number;
    address: string;
  };
  markerClick!: (marker: Marker) => void;
  select!: Record<string, boolean>;

  @ViewChild(MapInfoWindow) infoWindow!: MapInfoWindow;

  constructor(modal: NgbActiveModal) {
    super(modal);
  }

  ngOnInit(): void {
    this.prepareMap();
  }

  ngAfterViewInit(): void {
    this.infoWindow.closeclick.pipe(takeUntil(this._destroy)).subscribe(() => {
      this.activeMarker = undefined;
    });
  }

  open(marker: Marker, anchor: MapMarker) {
    this.activeMarker = marker;
    this.infoWindow.open(anchor);
  }

  private prepareMap() {
    const markers: Marker[] = [];

    this.data.forEach((el) => {
      const address = el.contact.contact_address[0];

      markers.push({
        position: {
          lat: parseFloat(getPropValue(address, 'address.latitude') as string),
          lng: parseFloat(getPropValue(address, 'address.longitude') as string),
        },
        name: getPropValue(el, 'contact.__str__') as string,
        description: getPropValue(address, 'address.__str__') as string,
        iconUrl: BLUE_MARKER_SRC,
        id: getPropValue(el, 'id') as string,
        selected: this.select[getPropValue(el, 'id') as string],
      });
    });

    markers.push({
      position: {
        lat: this.supportData.latitude,
        lng: this.supportData.longitude,
      },
      name: this.supportData.__str__,
      description: this.supportData.address,
      iconUrl: RED_MARKER_SRC,
    });

    this.markers = markers;

    this.mapCenter = {
      lat: this.supportData.latitude,
      lng: this.supportData.longitude,
    };
  }

  ngOnDestroy(): void {
    this._destroy.next();
    this._destroy.complete();
  }
}
