import {
  Component,
  ChangeDetectionStrategy,
  ViewChild,
  ElementRef,
  AfterViewInit,
} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { getEmailVariablesValue } from '@webui/data';
import { Formatter } from '@webui/utilities';

@Component({
  selector: 'webui-email-preview',
  templateUrl: './email-preview.component.html',
  styleUrls: ['./email-preview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EmailPreviewComponent implements AfterViewInit {
  template?: string;

  @ViewChild('iframe') iframe?: ElementRef<HTMLIFrameElement>;

  constructor(private modal: NgbActiveModal) {}

  ngAfterViewInit(): void {
    const formatter = new Formatter('[[', ']]');
    const template = this.template || '';
    const iframeElement = this.iframe?.nativeElement;

    if (!iframeElement) {
      return;
    }

    iframeElement.src = 'about:blank';
    iframeElement.contentWindow?.document.open();
    iframeElement.contentWindow?.document.write(
      formatter.format(template, getEmailVariablesValue())
    );
  }

  onClose(): void {
    this.modal.close();
  }
}
