import { ActivatedRoute } from '@angular/router';
import {
  Component,
  OnInit,
  Output,
  EventEmitter,
  OnDestroy,
  ChangeDetectionStrategy,
} from '@angular/core';
import {
  BehaviorSubject,
  delay,
  distinctUntilChanged,
  merge,
  Subject,
  takeUntil,
} from 'rxjs';

import { FilterService } from '../../../services';
import { FormControl } from '@angular/forms';

type Option = {
  label: string;
  value: string;
};

@Component({
  selector: 'webui-filter-select',
  templateUrl: 'filter-select.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FilterSelectComponent implements OnInit, OnDestroy {
  private _destroy = new Subject<void>();
  private _options = new BehaviorSubject<Option[] | null>(null);

  public config: any;

  @Output()
  public event: EventEmitter<any> = new EventEmitter();

  control = new FormControl('');

  options$ = this._options.asObservable();
  hasQuery$ = this.control.valueChanges;

  constructor(private fs: FilterService, private route: ActivatedRoute) {}

  public ngOnInit() {
    this._options.next([...this.config.options]);
    this.control.patchValue(this.config.default || '');

    merge(this.route.queryParams, this.fs.reset)
      .pipe(delay(200), takeUntil(this._destroy))
      .subscribe(() => this.updateFilter());

    this.control.valueChanges
      .pipe(distinctUntilChanged(), takeUntil(this._destroy))
      .subscribe((value) => {
        this.onChange(value);
      });
  }

  public ngOnDestroy() {
    this._destroy.next();
    this._destroy.complete();
  }

  public onChange(value: string) {
    this.fs.generateQuery(
      this.genericQuery(this.config.query, value),
      this.config.key,
      this.config.listName,
      value
    );
    this.changeQuery();
  }

  public genericQuery(query: string, data: string) {
    return data ? `${query}=${data}` : '';
  }

  public changeQuery() {
    this.event.emit({
      list: this.config.listName,
    });
  }

  public parseQuery(query: string) {
    const value = query.split('=')[1];

    this.control.patchValue(value);
  }

  public updateFilter() {
    const data = this.fs.getQueries(this.config.listName, this.config.key);

    if (data) {
      if (data.byQuery) {
        this.parseQuery(data.query);
      } else {
        this.control.patchValue(data);
      }
    } else {
      this.control.patchValue('');
    }
  }

  public resetFilter() {
    this.fs.generateQuery('', this.config.key, this.config.listName);
    this.control.patchValue('');
  }

  public getTranslateKey(type: string): string {
    return `filter.${this.config.key}.${type}`;
  }
}
