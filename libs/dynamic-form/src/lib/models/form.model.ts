import { BehaviorSubject, Subject } from 'rxjs';
import { FormMode } from '../services';

export interface IFormErrors {
  non_field_errors: string | string[];
  detail: string;
  [key: string]: string | string[];
}

let counter = 0;

export class Form {
  private _mode: BehaviorSubject<FormMode>;
  private _errors = new Subject<IFormErrors>();
  private _saveProcess = new BehaviorSubject<boolean>(false);
  private _id = counter++;
  private _initialData: { [key: string]: any } = {};
  private _additionalData: { [key: string]: any } = {};
  private _useToast: boolean;

  public allowMethods: string[];
  public hasTabs!: boolean;
  public hideEditButton!: boolean;
  public disableSaveButton = false;

  get mode() {
    return this._mode.asObservable();
  }

  get saveProcess() {
    return this._saveProcess.asObservable();
  }

  get id() {
    return this._id;
  }

  get errors$() {
    return this._errors.asObservable();
  }

  get initialData() {
    return { ...this._initialData };
  }

  get additionalData() {
    return { ...this._additionalData };
  }

  get useToast() {
    return this._useToast;
  }

  public endpoint: string;

  constructor(
    endpoint: string,
    mode: FormMode,
    allowMethods: string[],
    useToast = false
  ) {
    this._mode = new BehaviorSubject(mode);
    this.endpoint = endpoint;
    this.allowMethods = allowMethods;
    this._useToast = useToast;
  }

  public changeMode(mode: FormMode): void {
    this._mode.next(mode);
  }

  public setSaveProcess(saving: boolean): void {
    this._saveProcess.next(saving);
  }

  public setErrors(errors: IFormErrors): void {
    this._errors.next(errors);
  }

  public setInitialData(data: any) {
    this._initialData = { ...this._initialData, ...data };
  }

  public updateAdditionalData(key: string, value: any) {
    this._additionalData = {
      ...this._additionalData,
      [key]: value,
    };
  }
}
