import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import {
  ModalDismissReasons,
  NgbActiveModal,
} from '@ng-bootstrap/ng-bootstrap';

export type FlatDialogConfig = {
  hasCloseButton: boolean;
};

const defaultConfig: FlatDialogConfig = {
  hasCloseButton: true,
};

@Component({
  selector: 'webui-flat-dialog',
  templateUrl: './flat-dialog.component.html',
  styleUrls: ['./flat-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FlatDialogComponent {
  @Input() config?: FlatDialogConfig;

  get dialogConfig() {
    return this.config
      ? {
          ...defaultConfig,
          ...this.config,
        }
      : defaultConfig;
  }

  constructor(private active: NgbActiveModal) {}

  public dismiss() {
    this.active.dismiss(ModalDismissReasons.ESC);
  }

  public close(result?: unknown) {
    return this.active.close(result);
  }
}
