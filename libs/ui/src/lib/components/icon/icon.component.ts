import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

type Icon =
  | 'filter'
  | 'person'
  | 'company'
  | 'calendar'
  | 'jobsite'
  | 'position'
  | 'clock'
  | 'close'
  | 'timer'
  | 'arrow-down'
  | 'arrow-up'
  | 'plus'
  | 'delete'
  | 'star-filled'
  | 'star'
  | 'add-picture'
  | 'edit'
  | 'drag';

@Component({
  selector: 'webui-svg-icon',
  templateUrl: './icon.component.html',
  styleUrls: ['./icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IconComponent {
  @Input() icon!: Icon;
  @Input() size?: 'sm' | 'md' = 'md';

  get svgId(): string {
    return `#icon-${this.icon}`;
  }
}
