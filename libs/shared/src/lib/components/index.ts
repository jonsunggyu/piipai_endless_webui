import { SiteLoaderComponent } from './site-loader/site-loader.component';
import { DatepickerComponent } from './datepicker/datepicker.component';
import { DateRangeComponent } from './datepicker/date-range/date-range.component';

export const components = [
  SiteLoaderComponent,
  DatepickerComponent,
  DateRangeComponent,
];
