import { Component } from '@angular/core';

@Component({
  selector: 'webui-client',
  template: `<router-outlet></router-outlet>`,
})
export class ClientComponent {}
